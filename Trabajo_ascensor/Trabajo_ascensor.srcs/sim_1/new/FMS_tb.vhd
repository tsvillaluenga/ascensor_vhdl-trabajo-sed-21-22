-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 12.12.2021 18:49:24 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_fsm is
end tb_fsm;

architecture tb of tb_fsm is

    component fsm
        port (RESET      : in std_logic;
              CLK        : in std_logic;
              PUSHBUTTON : in std_logic_vector (0 to 3);
              CODE       : out std_logic_vector (0 to 2);
              MOTOR      : out std_logic_vector (0 to 1);
              PUERTA     : out std_logic);
    end component;

    signal RESET      : std_logic;
    signal CLK        : std_logic;
    signal PUSHBUTTON : std_logic_vector (0 to 3);
    signal CODE       : std_logic_vector (0 to 2);
    signal MOTOR      : std_logic_vector (0 to 1);
    signal PUERTA     : std_logic;

    constant TbPeriod : time := 1000 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : fsm
    port map (RESET      => RESET,
              CLK        => CLK,
              PUSHBUTTON => PUSHBUTTON,
              CODE       => CODE,
              MOTOR      => MOTOR,
              PUERTA     => PUERTA);

    -- Generación del reloj
    TbClock <= not TbClock after TbPeriod/40 when TbSimEnded /= '1' else '0';
    CLK <= TbClock;

    stimuli : process
    begin
        -- Inicialización
        PUSHBUTTON <= (others => '0');
        RESET <= '1';
        
        
        -- Código de prueba
        
        PUSHBUTTON <= "0010";
        wait for 100 ns;
        
        PUSHBUTTON <= "0001";
        wait for 100 ns;
        
        PUSHBUTTON <= "0100";
        wait for 100 ns;
        
        -- Prueba del Reset
        RESET <= '0';
        wait for 100 ns;
        RESET <= '1';
        wait for 100 ns;
        
        PUSHBUTTON <= "1000";
        wait for 100 ns;
        
        PUSHBUTTON <= "0100";
        wait for 100 ns;


        -- Para el reloj y termina la simulación
        TbSimEnded <= '1';
        wait;
    end process;

end tb;