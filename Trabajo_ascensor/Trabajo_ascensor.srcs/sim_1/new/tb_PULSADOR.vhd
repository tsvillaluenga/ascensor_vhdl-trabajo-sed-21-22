-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 19.12.2021 22:16:41 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_PULSADOR is
end tb_PULSADOR;

architecture tb of tb_PULSADOR is

    component PULSADOR
        port (BOT_IN  : in std_logic;
              BOT_OUT : out std_logic;
              CLK     : in std_logic;
              RESET   : in std_logic;
              BOT : out std_logic);
    end component;

    signal BOT_IN  : std_logic;
    signal BOT_OUT : std_logic;
    signal CLK     : std_logic;
    signal RESET   : std_logic;
    signal BOT : std_logic;

    constant TbPeriod : time := 25 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : PULSADOR
    port map (BOT_IN  => BOT_IN,
              BOT_OUT => BOT_OUT,
              CLK     => CLK,
              RESET   => RESET);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK is really your main clock signal
    CLK <= TbClock;

    stimuli : process
    begin
        -- Inicialización
        BOT_IN <= '0';
        RESET <= '1';
        
        
        -- Código de prueba
        wait for 50 ns;
        BOT_IN <= '1';
        wait for 50 ns;
        
        BOT_IN <= '0';
        wait for 100 ns;
        
        BOT_IN <= '1';
        wait for 50 ns;
        RESET <= '1';
        wait for 50 ns;
        BOT_IN <= '1';
        wait for 50 ns;
        
        BOT_IN <= '0';
        wait for 100 ns;
        
        BOT_IN <= '1';
        wait for 50 ns;
        
        -- Prueba del Reset
        RESET <= '0';
        wait for 50 ns;
        RESET <= '1';
        wait for 50 ns;
        
        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;