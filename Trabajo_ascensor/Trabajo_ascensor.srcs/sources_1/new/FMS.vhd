-- C�digo que gestiona la m�quina de estados finita

-------------------------------
-- Librer�as

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;
-------------------------------


-- Entidad de la m�quina de estados finita
entity fsm is
    port (
            RESET : in std_logic; -- Bot�n de Reset
            CLK : in std_logic; -- Se�al de reloj
            PUSHBUTTON : in std_logic_vector(0 TO 3); -- Pulsador de los pisos // BIT0-P1 / BIT1-P2 / BIT2-P3 / BIT3-P4
            MOTOR : out std_logic_vector(0 TO 1); -- Salida del motor: 00 parado, 01 baja y 10 sube  // bit0 - in1 / bit1 - in2
            PUERTA : out std_logic_vector(0 TO 3); -- Puerta del ascensor: 1 abierto y 0 cerrado // BIT0-P1 / BIT1-P2 / BIT2-P3 / BIT3-P4
            DISPLAY : out std_logic_vector(6 DOWNTO 0);
            
            -- Se�ales sensor ultrasonico
            sensor_eco: in std_logic;
            sensor_TRIGGER : out std_logic
          );
end fsm;


-- Arquitectura behavioral de la m�quina de estados
architecture behavioral of fsm is


component Ultrasonidos is
port( CLK     : IN  STD_LOGIC;
      ECO     : IN  STD_LOGIC;
      TRIGGER : OUT STD_LOGIC;
      SALIDA     : OUT STD_LOGIC
    );
end component;

  
    type STATES is (S1, S2, S3, S4, S5, S6, S7, S8, S9, S10); -- Estados 1, 2, 3 y 4 para cada piso, estado 5 para subir y 6 para bajar
    signal current_state: STATES := S1;
    signal next_state: STATES;
    signal piso: std_logic_vector(0 TO 3); -- Se�al que indica el piso en el que est�
    signal ULTRASONIDO: std_logic; -- Se�al salida ultrasonido (1 - Activo / 0 - No Activo)

    
begin


  --Ultrasonidos
  
  Inst_US: Ultrasonidos port map(
      CLK => clk,
      ECO => sensor_eco,
      TRIGGER => sensor_trigger,
      SALIDA => ULTRASONIDO
    );
    
    
    -- Proceso que gestiona las se�ales de Reset y el cambio de estado en cada flanco de subida del reloj
    state_register: process (RESET, CLK)
    begin
        if (RESET='0') then
            current_state <= S1; -- Pasa al estado 1 si se pulsa el bot�n de Reset
        elsif (rising_edge(CLK)) then
            current_state <= next_state; -- Actualiza el estado a cada flanco de subida del reloj
        end if;
    end process;
    
   -- Proceso que gestiona los cambios de estado
    nextstate_decod: process (PUSHBUTTON, next_state, clk)
    begin
        next_state <= current_state;

        case current_state is
            -- Los estados 1, 2, 3 y 4 pasan al estado de bajada si se pulsa el bot�n de un piso inferior y al de subida si se pulsa el bot�n de un piso superior
            when S1 =>
                      if PUSHBUTTON = "0100" then
                        piso <= "0100";
                        next_state <= S5;
                      elsif PUSHBUTTON = "0010" then
                        piso <= "0010";
                        next_state <= S6;
                      elsif PUSHBUTTON = "0001" then
                        piso <= "0001";
                        next_state <= S7;
                      end if;
            when S2 =>
                      if PUSHBUTTON = "1000" then
                        piso <= "1000";
                        next_state <= S8;
                      elsif PUSHBUTTON = "0010" then
                        piso <= "0010";
                        next_state <= S6;
                      elsif PUSHBUTTON = "0001" then
                        piso <= "0001";
                        next_state <= S7;
                      end if;
            when S3 =>
                      if PUSHBUTTON = "1000" then
                        piso <= "1000";
                        next_state <= S8;
                      elsif PUSHBUTTON = "0100" then
                        piso <= "0100";
                        next_state <= S9;
                      elsif PUSHBUTTON = "0001" then
                        piso <= "0001";
                        next_state <= S7;
                      end if;
            when S4 =>
                      if PUSHBUTTON = "1000" then
                        piso <= "1000";
                        next_state <= S8;
                      elsif PUSHBUTTON = "0100" then
                        piso <= "0100";
                        next_state <= S9;
                      elsif PUSHBUTTON = "0010" then
                        piso <= "0010";
                        next_state <= S10;
                      end if;
                      
            -- Estados de subida
            when S5 =>
                      if (ULTRASONIDO = '1') then
                        next_state <= S2;
                      end if;
            when S6 =>
                      if (ULTRASONIDO = '1') then
                        next_state <= S3;
                      end if;
            when S7 =>
                      if (ULTRASONIDO = '1') then
                        next_state <= S4;
                      end if;
                      
            -- Estados de bajada
            when S8 =>
                      if (ULTRASONIDO = '1') then
                        next_state <= S1;
                      end if;  
            when S9 =>
                      if (ULTRASONIDO = '1') then
                        next_state <= S2;
                      end if;
            when S10 =>
                      if (ULTRASONIDO = '1') then
                        next_state <= S3;
                      end if;        
            end case;
    end process;
    
    -- Proceso que gestiona las salidas en funci�n del estado
    output_decod: process (current_state)
    begin
        MOTOR <= (OTHERS => '0'); -- Motor parado
        PUERTA <= "0000"; --Puertas cerradas

        case current_state is
            when S1 =>
                      DISPLAY <= "1001111"; --PLANTA 1
                      PUERTA <= "1000"; -- Se abren las puertas PISO 1
            when S2 =>
                      DISPLAY <= "0010010"; --PLANTA 2
                      PUERTA <= "0100"; -- Se abren las puertas PISO 2
            when S3 =>
                      DISPLAY <= "0000110"; --PLANTA 3
                      PUERTA <= "0010"; -- Se abren las puertas PISO 3
            when S4 =>
                      DISPLAY <= "1001100"; --PLANTA 4
                      PUERTA <= "0001"; -- Se abren las puertas PISO 4
            when S5 =>
                      MOTOR <= "10"; -- Motor horario (SUBE)
                      PUERTA <= "0000"; -- Se cierran las puertas
                      DISPLAY <= "0100100"; --SUBIR (S)
            when S6 =>
                      MOTOR <= "10"; -- Motor horario (SUBE)
                      PUERTA <= "0000"; -- Se cierran las puertas
                      DISPLAY <= "0100100"; --SUBIR (S)
            when S7 =>
                      MOTOR <= "10"; -- Motor horario (SUBE)
                      PUERTA <= "0000"; -- Se cierran las puertas
                      DISPLAY <= "0100100"; --SUBIR (S)
            when S8 =>
                      MOTOR <= "01"; -- Motor antihorario (BAJA)
                      PUERTA <= "0000"; -- Se cierran las puertas
                      DISPLAY <= "0000000"; --BAJAR (B)
            when S9 =>
                      MOTOR <= "01"; -- Motor antihorario (BAJA)
                      PUERTA <= "0000"; -- Se cierran las puertas
                      DISPLAY <= "0000000"; --BAJAR (B)
            when S10 =>
                      MOTOR <= "01"; -- Motor antihorario (BAJA)
                      PUERTA <= "0000"; -- Se cierran las puertas
                      DISPLAY <= "0000000"; --BAJAR (B)

        end case;
    end process;
    
end behavioral;