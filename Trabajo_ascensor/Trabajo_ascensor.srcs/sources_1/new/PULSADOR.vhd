
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity PULSADOR is
    Port ( BOT_IN  : in  STD_LOGIC;
           BOT_OUT : out STD_LOGIC;
           CLK     : in  STD_LOGIC
           );          
end PULSADOR;


architecture Behavioral of PULSADOR is

component SYNC is
    Port ( CLK : in STD_LOGIC;
           ASYNC_IN : in STD_LOGIC;
           SYNC_OUT : out STD_LOGIC
         );
end component;

component EDGE_Detect is
    Port ( CLK : in STD_LOGIC;
           SYNC_IN : in STD_LOGIC;
           EDGE : out STD_LOGIC
         );
end component;

signal boton_sync: std_logic;
signal edgeD: std_logic;

begin

  Inst_SYNC: SYNC port map (
    CLK => CLK,
    ASYNC_IN => BOT_IN ,
    SYNC_OUT => boton_sync
  );
  
  Inst_EDGE: EDGE_Detect port map (
    CLK => CLK,
    SYNC_IN => boton_sync ,
    EDGE => BOT_OUT
  );
  

end architecture;