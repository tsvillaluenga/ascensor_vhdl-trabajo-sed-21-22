
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SYNC is
    Port ( CLK      : in  STD_LOGIC;
           ASYNC_IN : in  STD_LOGIC;
           SYNC_OUT : out STD_LOGIC);
end SYNC;

architecture Behavioral of SYNC is
    signal sreg: std_logic_vector(1 downto 0);
begin

    process(CLK)
    begin
        if rising_edge (CLK) then
        SYNC_OUT <= sreg(1);
        sreg     <= sreg(0) & ASYNC_IN;
        end if;
    end process;

end Behavioral;
