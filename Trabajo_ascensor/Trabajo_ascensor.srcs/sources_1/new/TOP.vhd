library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity TOP is
  Port ( 
        clk: in std_logic;
        reset: in std_logic;
        botones: in std_logic_vector(1 to 4);
        digctrl : out std_logic_vector(7 DOWNTO 0);
        segment: out std_logic_vector(6 DOWNTO 0);
        led: out std_logic_vector(0 TO 3);
        --code: out std_logic_vector(0 to 2);
        motor: out std_logic_vector(0 to 1);
        sensor_eco: in std_logic;
        sensor_TRIGGER: out std_logic
        --sensor: in std_logic --Sensor switch
  );
end TOP;

architecture Behavioral of TOP is

component PULSADOR is
    Port ( BOT_IN  : in  STD_LOGIC;
           BOT_OUT : out STD_LOGIC;
           CLK     : in  STD_LOGIC
         );
end component;

component fsm is
    port (
            RESET : in std_logic; -- Bot�n de Reset
            CLK : in std_logic; -- Se�al de reloj
            PUSHBUTTON : in std_logic_vector(0 TO 3); -- Pulsador de los pisos (P1 P2 P3 P4)
            MOTOR : out std_logic_vector(0 TO 1); -- Salida del motor: 00 parado, 01 baja y 11 sube
            PUERTA : out std_logic_vector(0 TO 3); -- Puerta del ascensor: 0 abierto y 1 cerrado
            DISPLAY : out std_logic_vector(6 DOWNTO 0);
            sensor_eco: in std_logic;
            sensor_TRIGGER : out std_logic
            
          );
end component;



--SE�ALES:
signal boton_sync: std_logic_vector(1 to 4);
signal salida_US: std_logic;

begin

--PULSADORES:

  Inst_PULSADOR1: PULSADOR port map (
    CLK => clk,
    BOT_IN => botones(1),
    BOT_OUT => boton_sync(1)
  );
  
    Inst_PULSADOR2: PULSADOR port map (
    CLK => clk,
    BOT_IN => botones(2),
    BOT_OUT => boton_sync(2)
    );
  
    Inst_PULSADOR3: PULSADOR port map (
    CLK => clk,
    BOT_IN => botones(3),
    BOT_OUT => boton_sync(3)
  );
  
    Inst_PULSADOR4: PULSADOR port map (
    CLK => clk,
    BOT_IN => botones(4),
    BOT_OUT => boton_sync(4)
  );
  

 --FMS:
 
    Inst_FMS: fsm port map (
      RESET => reset,
      CLK => clk,
      PUSHBUTTON => boton_sync,
      MOTOR => motor,
      PUERTA => led,
      DISPLAY => segment,
      sensor_TRIGGER => sensor_TRIGGER,
      sensor_eco => sensor_eco
  );



digctrl <= "11111110";

end Behavioral;