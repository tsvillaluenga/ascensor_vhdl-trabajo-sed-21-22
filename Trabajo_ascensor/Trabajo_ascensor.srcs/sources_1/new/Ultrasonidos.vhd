
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Ultrasonidos is
port( CLK     : IN  STD_LOGIC;
      ECO     : IN  STD_LOGIC;
      TRIGGER : OUT STD_LOGIC;
      SALIDA     : OUT STD_LOGIC
    );
end Ultrasonidos;

architecture Behavioral of Ultrasonidos is

signal eco_time: integer;

begin
  process(clk)
    variable c1,c2: integer:=0;
    variable y :std_logic:='1';
  begin
    if rising_edge(clk) then

        if(c1=0) then
            trigger<='1';
        elsif(c1=500) then--100us
            trigger<='0';
            y:='1';
        elsif(c1=5000000) then-- 100 ms
            c1:=0;
            trigger<='1';
        end if;
        c1:=c1+1;

        if(eco = '1') then
            c2:=c2+1;
        elsif(eco = '0' and y='1' ) then-- I change the y to not get echo_time =0;
            eco_time<= c2;
            c2:=0;
            y:='0';
        end if;

        if(eco_time < 50000) then--10 cm
           Salida <='1';
        else 
           Salida <='0';
        end if;
    end if; 
end process ;


end Behavioral;
